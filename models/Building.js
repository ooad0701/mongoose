const mongoose = require('mongoose')
const { Schema } = mongoose
const buildingSchema = Schema({
  name: String,
  floor: Number,
  rooms: [{ type: Schema.Types.ObjectId, ref: 'Room' }]
})
module.exports = mongoose.model('Building', buildingSchema)
