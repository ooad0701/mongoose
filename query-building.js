const mongoose = require('mongoose')
const Room = require('./models/Room')
const Building = require('./models/Building')
mongoose.connect('mongodb://localhost:27017/example')

async function main () {
  // update
  // const room = await Room.findById('621cd9f689558d7fa539f365')
  // room.capacity = 20
  // room.save()
  // console.log(room)
  const room = await Room.findOne({ capacity: { $lt: 100 } })
  console.log(room)
  console.log('-------------------')
  const rooms = await Room.find({ capacity: { $lt: 100 } })
  console.log(rooms)
  const buildings = await Building.find({})
  console.log(JSON.stringify(buildings))
}

main().then(() => {
  console.log('finish')
})
